package de.ithappens.calories.database.engine;

import java.util.UUID;

public interface EngineInterface {

    public abstract String PREFIX();

    public static String generateId(String prefix) {
        return prefix + "__" + UUID.randomUUID().toString();
    }

}
