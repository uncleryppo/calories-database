package de.ithappens.calories.database.engine;

import de.ithappens.calories.database.model.generated.tables.daos.ConsumptionDao;
import de.ithappens.calories.database.model.generated.tables.pojos.Consumption;
import org.apache.commons.lang3.StringUtils;
import org.jooq.Configuration;

import java.util.*;

public class Consumption_engine extends ConsumptionDao implements EngineInterface {
    @Override
    public String PREFIX() {
        return "CONS";
    }

    public Consumption_engine(Configuration configuration) {
        super(configuration);
    }

    @Override
    public void insert(Consumption consumption) {
        if (consumption != null) {
            consumption.setId(EngineInterface.generateId(PREFIX()));
            super.insert(consumption);
        }
    }

    @Override
    public void insert(Collection<Consumption> consumptions) {
        Optional<Collection<Consumption>> optionalConsumptions = Optional.of(consumptions);
        optionalConsumptions.ifPresent(cons ->
                {
                    for (Consumption con : cons) {
                        con.setId(EngineInterface.generateId(PREFIX()));
                    }
                    super.insert(cons);
                }
        );
    }

    public void updateOrInsert(Collection<Consumption> consumptions) {
        List<Consumption> toInsert = new ArrayList<>();
        List<Consumption> toUpdate = new ArrayList<>();
        for (Consumption toDecide : consumptions) {
            if (StringUtils.isEmpty(toDecide.getId())) {
                toInsert.add(toDecide);
            } else {
                toUpdate.add(toDecide);
            }
        }
        if (toInsert.size() > 0) {
            insert(toInsert);
        }
        update(toUpdate);
    }

}
