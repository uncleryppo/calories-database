package de.ithappens.calories.database;

import de.ithappens.calories.database.engine.Consumption_engine;
import de.ithappens.calories.database.model.generated.tables.daos.ConsumptionDao;
import de.ithappens.calories.database.model.generated.tables.pojos.Consumption;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class CaloriesDatabase {

    private final static String H2_DRIVER_NAME = "org.h2.Driver";
    private final static String H2_PROTOCOL = "jdbc:h2:";
    private String h2fileLocation;
    private DSLContext dsl = null;
    private Consumption_engine consumptionEngine;

    public CaloriesDatabase(String h2fileLocation) throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
        this.h2fileLocation = h2fileLocation;
        Driver driver = (Driver) Class.forName(H2_DRIVER_NAME).newInstance();
        DriverManager.registerDriver(driver);
        Connection con = DriverManager.getConnection(H2_PROTOCOL + h2fileLocation);
        dsl = DSL.using(con, SQLDialect.H2);
    }

    public Consumption_engine ConsumptionEngine() {
        return consumptionEngine != null
        ? consumptionEngine
        : (consumptionEngine = new Consumption_engine(dsl.configuration()));
    }

}
